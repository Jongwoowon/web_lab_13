# Answers to exercise 1 questions
# A. Display the departments offering courses
SELECT DISTINCT dept
FROM unidb_courses
ORDER BY dept;

# B. Display the semesters being attended

SELECT DISTINCT semester
FROM unidb_attend
ORDER BY semester;

# C. Display the courses that are attended
SELECT DISTINCT
  dept,
  num
FROM unidb_attend
ORDER BY dept, num;

# D. List the student names and country, ordered by first name
SELECT
  fname,
  lname,
  country
FROM unidb_students
ORDER BY fname;

# E. List the student names and mentors, ordered by mentors
SELECT
  s.fname,
  s.lname,
  m.fname,
  m.lname
FROM unidb_students AS s, unidb_students AS m
WHERE s.mentor = m.id
ORDER BY m.lname;

# F. List the lecturers, ordered by office
SELECT
  office,
  fname,
  lname
FROM unidb_lecturers
ORDER BY office;

# G. List the staff whose staff number is greater than 500
SELECT staff_no, fname, lname
FROM unidb_lecturers
WHERE staff_no > 500;

# H. List the students whose id is greater than 1668 and less than 1824
SELECT id, fname, lname
FROM unidb_students
WHERE 1824 > id AND id > 1668;

# I. List the students from NZ, Australia and US
SELECT fname, lname
FROM unidb_students
WHERE country = 'NZ' OR country = 'AU' OR country = 'US';

# J. List the lecturers in G Block
SELECT fname,lname
FROM unidb_lecturers
WHERE office LIKE 'G%';

# K. List the courses not from the Computer Science Department
SELECT *
FROM unidb_courses
WHERE dept != 'comp';

# L. List the students from France or Mexico
SELECT *
FROM unidb_students
WHERE country = 'FR' OR  country = 'MX';
