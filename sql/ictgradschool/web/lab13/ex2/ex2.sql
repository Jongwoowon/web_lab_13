# Answers to exercise 2 questions

# A. What are the names of the students who attend COMP219?
SELECT
  s.fname,
  s.lname,
  a.dept,
  a.num
FROM unidb_students AS s, unidb_attend AS a
WHERE a.id = s.id AND a.dept = 'comp' AND a.num = 219
ORDER BY a.num;

# B. What are the names of the student reps that are not from NZ?

SELECT
  s.fname,
  s.lname,
  s.country
FROM unidb_students AS s, unidb_courses AS c
WHERE s.id = c.rep_id AND s.country != 'NZ';

# C. Where are the offices for the lecturers of 219?

SELECT DISTINCT
  l.fname,
  l.lname,
  l.office
FROM unidb_lecturers AS l, unidb_teach AS t
WHERE l.staff_no = t.staff_no AND t.num = '219';

# D. What are the names of the students taught by Te Taka?

SELECT
  l.fname,
  s.fname,
  a.num
FROM unidb_lecturers AS l, unidb_students AS s, unidb_attend AS a, unidb_teach AS t
WHERE l.staff_no = t.staff_no AND t.dept = a.dept AND t.num = a.num AND s.id = a.id AND l.fname = 'Te Taka'
ORDER BY s.fname;

# E. List the students and their mentors

SELECT
  s.fname,
  s.lname,
  m.fname,
  m.lname
FROM unidb_students s, unidb_students m
WHERE m.id = s.mentor;

# F. Name the lecturers whose office is in G-Block as well naming the students that are not from NZ

SELECT
  fname,
  lname,
  office
FROM unidb_lecturers
WHERE office LIKE 'G%'
UNION
SELECT
  fname,
  lname,
  country
FROM unidb_students
WHERE country != 'NZ';

# G. List the course co-ordinator and student rep for COMP219

SELECT DISTINCT
  c.dept, c.num, c.coord_no, l.fname, l.fname, c.rep_id, s.fname, s.lname
FROM unidb_lecturers AS l, unidb_students AS s, unidb_courses AS c, unidb_teach AS t
WHERE c.dept = 'comp' AND c.num = 219 AND c.coord_no = l.staff_no AND c.rep_id = s.id;